**My learning materials**
Quick sort in javascript with array destructuring. A simple example.
There is all javascript inside html among <script></script> tags

---

## Use Live server

to console log in the browser console.

---

## Array destructuring princip:

array destructuring princip:
const names = ['ania', 'ben', 'tom']; const [first, second] = names;
=> console.log(first); /-> 'ania'

let aa;
let bb;

[aa, bb] = [1, 2, 3]; // aa => 1, bb => 2.

// swapping a and b:
let a = 1;
let b = 2;

[a, b] = [b, a]; // a => 2, b => 1.

explanation:
created temporary array [b, a] (which is [2, 1])
Destructuring of the temporary array occurs: [a, b] = [2, 1]
The swapping of a and b => variable a = 2, and b= 1

---

second way:
let a = 1;
let b = 2;

a = a + b;
b = a - b;
a = a - b;

a; // => 2
b; // => 1

a = a + b assigns to a the value 1 + 2.
b = a - b assigns to b the value 1 + 2 - 2 = 1 (b is now 1).
a = a - b assigns to a the value 1 + 2 - 1 = 2 (a is now 2).
\*/
